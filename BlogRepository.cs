namespace Blog
{
    using System;
    using System.Collections.Generic;

    using Blog.Models;

    using Microsoft.Extensions.Logging;
    using System.Linq;

    using Blog.Dtos;

    using Microsoft.EntityFrameworkCore;

    internal interface IBlogRepository
    {
        public IEnumerable<string> AllPosts();
        public IEnumerable<NumberOfCommentsPerUserDto> NumberOfCommentsPerUser();
        public IEnumerable<PostsOrderedByLastCommentDateDto> PostsOrderedByLastCommentDate();
        public IEnumerable<NumberOfLastCommentsLeftByUserDto> NumberOfLastCommentsLeftByUser();
    }

    public class BlogMyDbRepository : IBlogRepository
    {
        private static MyDbContext _context;

        public BlogMyDbRepository()
        {
            var loggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });
            _context = new MyDbContext(loggerFactory);
            _context.Database.EnsureCreated();
            InitializeData(_context);
        }

        public IEnumerable<string> AllPosts()
        {
            var data = _context.BlogPosts.Select(x => x.Title).ToList();
            return data;
        }

        public IEnumerable<NumberOfCommentsPerUserDto> NumberOfCommentsPerUser()
        {
            var numberOfCommentsPerUser = _context.BlogComments
                .GroupBy(comment => comment.UserName)
                .Select(comment =>
                    new NumberOfCommentsPerUserDto
                        { Name = comment.Key, Count = comment.Count() });
            return numberOfCommentsPerUser;
        }

        public IEnumerable<PostsOrderedByLastCommentDateDto> PostsOrderedByLastCommentDate()
        {
            var orderedPosts = _context.BlogPosts
                .Include(post => post.Comments)
                .Select(post => new PostsOrderedByLastCommentDateDto
                {
                    Post = post.Title, 
                    LastComment = post.Comments
                        .OrderByDescending(comment => comment.CreatedDate)
                        .FirstOrDefault()
                });
            return orderedPosts;
        }

        public IEnumerable<NumberOfLastCommentsLeftByUserDto> NumberOfLastCommentsLeftByUser()
        {
            var userLastCommentCounts = PostsOrderedByLastCommentDate()
                .AsEnumerable()
                .GroupBy(post => post.LastComment.UserName)
                .Select(post => new NumberOfLastCommentsLeftByUserDto
                {
                    UserName = post.Key,
                    Count = post.Count()
                });
            return userLastCommentCounts;
        }

        private static void InitializeData(MyDbContext context)
        {
            context.BlogPosts.Add(new BlogPost("Post1")
            {
                Comments = new List<BlogComment>()
                {
                    new BlogComment("1", new DateTime(2020, 3, 2), "Petr"),
                    new BlogComment("2", new DateTime(2020, 3, 4), "Elena"),
                    new BlogComment("8", new DateTime(2020, 3, 5), "Ivan"),
                }
            });
            context.BlogPosts.Add(new BlogPost("Post2")
            {
                Comments = new List<BlogComment>()
                {
                    new BlogComment("3", new DateTime(2020, 3, 5), "Elena"),
                    new BlogComment("4", new DateTime(2020, 3, 6), "Ivan"),
                }
            });
            context.BlogPosts.Add(new BlogPost("Post3")
            {
                Comments = new List<BlogComment>()
                {
                    new BlogComment("5", new DateTime(2020, 2, 7), "Ivan"),
                    new BlogComment("6", new DateTime(2020, 2, 9), "Elena"),
                    new BlogComment("7", new DateTime(2020, 2, 10), "Ivan"),
                    new BlogComment("9", new DateTime(2020, 2, 14), "Petr"),
                }
            });
            context.SaveChanges();
        }
    }
}