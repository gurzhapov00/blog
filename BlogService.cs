namespace Blog
{
    using System.Collections.Generic;
    using System.Linq;

    using Blog.Dtos;

    internal interface IBlogService
    {
        public IEnumerable<string> AllPosts();
        public IEnumerable<NumberOfCommentsPerUserDto> NumberOfCommentsPerUser();
        public IEnumerable<PostsOrderedByLastCommentDateDto> PostsOrderedByLastCommentDate();
        public IEnumerable<NumberOfLastCommentsLeftByUserDto> NumberOfLastCommentsLeftByUser();
    }

    public class BlogService : IBlogService
    {
        private static IBlogRepository _repository;

        public BlogService()
        {
            _repository = new BlogMyDbRepository();
        }

        public IEnumerable<string> AllPosts()
        {
            return _repository.AllPosts().ToList();
        }

        public IEnumerable<NumberOfCommentsPerUserDto> NumberOfCommentsPerUser()
        {
            return _repository.NumberOfCommentsPerUser().ToList();
        }

        public IEnumerable<PostsOrderedByLastCommentDateDto> PostsOrderedByLastCommentDate()
        {
            return _repository.PostsOrderedByLastCommentDate().ToList();
        }

        public IEnumerable<NumberOfLastCommentsLeftByUserDto> NumberOfLastCommentsLeftByUser()
        {
            return _repository.NumberOfLastCommentsLeftByUser().ToList();
        }
    }
}