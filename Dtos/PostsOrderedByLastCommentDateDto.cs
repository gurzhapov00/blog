namespace Blog.Dtos
{
    using Blog.Models;

    public class PostsOrderedByLastCommentDateDto
    {
        public string Post { get; set; }
        public BlogComment LastComment { get; set; }
    }
}