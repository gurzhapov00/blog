namespace Blog.Dtos
{
    public class NumberOfCommentsPerUserDto
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}