namespace Blog.Dtos
{
    public class NumberOfLastCommentsLeftByUserDto
    {
        public string UserName{ get; set; }
        public int Count { get; set; }
    }
}