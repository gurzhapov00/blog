﻿namespace Blog
{
    using System;
    using System.Text.Json;

    internal static class Program
    {
        private static void Main(string[] args)
        {
            var blogService = new BlogService();
            Console.WriteLine("Hello World!");

            Console.WriteLine("All posts:");
            Console.WriteLine(JsonSerializer.Serialize(blogService.AllPosts()));

            Console.WriteLine("How many comments each user left:");
            foreach (var userComment in blogService.NumberOfCommentsPerUser())
            {
                Console.WriteLine($"{userComment.Name}: {userComment.Count}");
            }
            // Expected result (format could be different, e.g. object serialized to JSON is ok):
            // Ivan: 4
            // Petr: 2
            // Elena: 3

            Console.WriteLine("Posts ordered by date of last comment. Result should include text of last comment:");
            foreach (var post in blogService.PostsOrderedByLastCommentDate())
            {
                Console.WriteLine($"{post.Post}: {post.LastComment.CreatedDate:yyyy-MM-dd}, {post.LastComment.Text}");
            }
            // Expected result (format could be different, e.g. object serialized to JSON is ok):
            // Post1: '2020-03-05', '8'
            // Post2: '2020-03-06', '4'
            // Post3: '2020-02-14', '9'

            Console.WriteLine("How many last comments each user left:");
            foreach (var userLastCommentCount in blogService.NumberOfLastCommentsLeftByUser())
            {
                Console.WriteLine($"{userLastCommentCount.UserName}: {userLastCommentCount.Count}");
            }
            // Expected result (format could be different, e.g. object serialized to JSON is ok):
            // Ivan: 2
            // Petr: 1
        }
    }
}